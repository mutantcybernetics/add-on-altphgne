# Add-on-ALTPHGNE

# Arduino + Radio + WiFi + Blutooth + Pressure + Humidity + Temperature + Gyro

## Compatible with mutantC v3/v2
Can able to turn off each module separately using Pi GPIO.

It has this module support (Click the blue color name to show the module)
- [Sparkfun Pro Micro](https://www.ebay.com/sch/i.html?_nkw=Pro-micro-3-3V) (3v)
- [LORA-RFM95](https://www.ebay.com/sch/i.html?_nkw=RFM95) (connected to pro micro)
- [NRF24L01](https://www.ebay.com/sch/i.html?_nkw=ESP8266+Serial+WIFI+Wireless+Transceiver+Module)
- [ESP-8266](https://www.ebay.com/sch/i.html?_nkw=ESP8266+Serial+WIFI+Wireless+Transceiver+Module)
- [BME280](https://www.ebay.com/sch/i.html?_nkw=bme280)
- [Gyro-MPU6050](https://www.ebay.com/sch/i.html?_nkw=mpu6050)


<img src="pic_main.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
